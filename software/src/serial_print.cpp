#include <Arduino.h>

void setup() {
    Serial2.begin(1200);
    delay(500);
    Serial2.println("Initing serial");
    pinMode(LED_BUILTIN, OUTPUT);

}

void loop() {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000);

    digitalWrite(LED_BUILTIN, LOW);
    Serial2.println("Blink low");
    delay(1000);
}