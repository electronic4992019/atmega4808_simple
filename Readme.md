# ATMEGA4808_SIMPLE

Simple project to test the ATMEGA4808 MCU. This is a relatively minimal PCB with :

- A diode
- A UPDI programming interface
- A serial programming interface (requires a bootloader)
- Some exposed ports (PD0 - PD7)

Should work in 5V as well as in 3.3v

The PCB has been engraved with a CNC

![board_empty](doc/board_empty.jpg)

![board_empty](doc/board_detail.jpg)

![board_empty](doc/board_finished.jpg)