#ifndef SOFTWARE_TASK_BLINK_H
#define SOFTWARE_TASK_BLINK_H

#include <Arduino.h>
#include "TaskScheduler.h"

// Callback methods for tasks
void BlinkFiveTimeCallback();

void BlinkForeverCallback();

// Tasks
Task TaskBlinkFiveTime(1000, 10, &BlinkFiveTimeCallback);
Task TaskBlinkForever(500, TASK_ONCE, &BlinkForeverCallback);
//
Scheduler runner;

void setup();

void loop();

#endif //SOFTWARE_TASK_BLINK_H
