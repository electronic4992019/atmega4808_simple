#include "task_blink.h"

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);

    runner.init();
    runner.addTask(TaskBlinkFiveTime);
    TaskBlinkFiveTime.enable();
}

void loop() {
    runner.execute();
}

bool b_led = LOW;

void BlinkFiveTimeCallback() {
    b_led = b_led == LOW ? HIGH : LOW;
    digitalWrite(LED_BUILTIN, b_led);
    if (TaskBlinkFiveTime.isLastIteration()) {
        runner.addTask(TaskBlinkForever);
        TaskBlinkForever.enable();
    }
}

void BlinkForeverCallback() {
    b_led = b_led == LOW ? HIGH : LOW;
    digitalWrite(LED_BUILTIN, b_led);
}